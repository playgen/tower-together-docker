FROM ubuntu:16.04

RUN apt-get -qq update && apt-get -y install nginx unzip joe
COPY . /usr/src/towertogether
WORKDIR /usr/src/towertogether

RUN ["mv", "-f", "/usr/src/towertogether/default", "/etc/nginx/sites-available/"]

RUN ["unzip", "GameServer.zip"]
RUN ["unzip", "WebGL.zip"]
RUN ["mv", "-f", "index.html", "WebGL/"]

RUN ["chmod", "+x", "GameServer.x86_64"]
RUN ["chmod", "+x", "startup.sh"]

# forward request and error logs to docker log collector
# RUN ["ln", "-sf", "/dev/stdout", "/var/log/nginx/access.log", "&&", "ln", "-sf", "/dev/stderr", "/var/log/nginx/error.log"]

EXPOSE 80
ENTRYPOINT ["./startup.sh"]